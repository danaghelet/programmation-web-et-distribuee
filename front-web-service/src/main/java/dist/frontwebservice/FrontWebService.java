package dist.frontwebservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

@RestController
public class FrontWebService {

    @Autowired
    DiscoveryClient discoveryClient;


    @GetMapping("/hello")
    public String hello() {
        List<ServiceInstance> instances = discoveryClient.getInstances("hello");
        ServiceInstance instance = instances.get(0);
        URI uri = instance.getUri();
        //String hostname = instance.getHost();
        int port = instance.getPort();
        RestTemplate restTemplate = new RestTemplate();
        try {
            String microservice1Address = "http://" + uri.toURL().toString() + ":" + port;
            ResponseEntity<String> response =
                    restTemplate.getForEntity(microservice1Address, String.class);
            String s = response.getBody();
            return s + "\n" + microservice1Address;
        }catch (Exception e){
            return "erreur lors de la conversion de l'URI";
        }

    }

    @GetMapping("/test")
    public String test(){
        return "le front web service tourne !";
    }
}
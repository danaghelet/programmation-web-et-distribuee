package dist.articlesService;

import org.springframework.data.repository.CrudRepository;

public interface ArticleRepository extends CrudRepository<Article, String> {

    public Article getArticleById(String id);
}

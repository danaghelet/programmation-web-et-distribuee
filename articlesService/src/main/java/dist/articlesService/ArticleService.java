package dist.articlesService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class ArticleService {

    ArticleRepository articleRepository;

    @Autowired
    public ArticleService( ArticleRepository articleRepository){
        super();
        this.articleRepository = articleRepository;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Article> getAllArticle() {
        return articleRepository.findAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void addArticle(@RequestBody Article article){
        articleRepository.save(article);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void removeArticle(@PathVariable String id){
        articleRepository.deleteById(id);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping( value = "/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Article getArticle(@PathVariable String id){
        return articleRepository.getArticleById(id);
    }


}

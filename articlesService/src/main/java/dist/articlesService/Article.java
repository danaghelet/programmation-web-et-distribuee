package dist.articlesService;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Article {
    private String id;
    private int prix;
    private String name;
    private String idVendeur;

    public Article(){
        this.id = "";
        this.prix = 0;
        this.name = "";
        this.idVendeur = "";
    }

    public Article(String id, int prix, String name, String idVendeur){
        this.id = id;
        this.name = name;
        this.prix = prix;
        this.idVendeur = idVendeur;
    }

    @Id
    public String getId() {
        return id;
    }

    public int getPrix() {
        return prix;
    }

    public String getName() {
        return name;
    }

    public String getIdVendeur() {
        return idVendeur;
    }

    public void setIdVendeur(String idVendeur) {
        this.idVendeur = idVendeur;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public void setName(String name) {
        this.name = name;
    }
}

#!/bin/bash

#Deployments
sudo kubectl apply -f ./deployment/registry-deployment.yml
sudo kubectl apply -f ./deployment/mongodb-deployment.yml
sudo kubectl apply -f ./deployment/article-service-deployment.yml
sudo kubectl apply -f ./deployment/client-service-deployment.yml
sudo kubectl apply -f ./deployment/gateway-deployment.yml
sudo kubectl apply -f ./deployment/front-deployment.yml


#Services
sudo kubectl apply -f ./service/registry-service.yml
sudo kubectl apply -f ./service/mongodb-service.yml
sudo kubectl apply -f ./service/article-service-service.yml
sudo kubectl apply -f ./service/client-service-service.yml
sudo kubectl apply -f ./service/gateway-service.yml
sudo kubectl apply -f ./service/front-service.yml

#Ingress

sudo kubectl apply -f ./front-to-back-ingress.yml

echo "Please wait until each service are available"
echo "You can check by running sudo kubectl get all"
echo "Then, access to website with http://"`sudo kubectl get service front-service | awk '{print $3}' | tail -1`"/"

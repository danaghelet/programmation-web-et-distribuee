package dist.clientService;

public class Mouvement {

    private long montant;
    private boolean versement;

    public Mouvement(long montant, boolean versement) {
        this.montant = montant;
        this.versement = versement;
    }

    public Mouvement() {
    }

    public long getMontant() {
        return montant;
    }

    public void setMontant(long montant) {
        this.montant = montant;
    }

    public boolean isVersement() {
        return versement;
    }

    public void setVersement(boolean versement) {
        this.versement = versement;
    }
}

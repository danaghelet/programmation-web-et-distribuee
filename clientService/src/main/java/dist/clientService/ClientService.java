package dist.clientService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;

@RestController
public class ClientService {

    ClientRepository clientRepository;

    @Autowired
    ClientService(ClientRepository clientRepository){
        super();
        this.clientRepository = clientRepository;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Client> getClients(){
        return clientRepository.findAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void addClient(@RequestBody Client client){
        clientRepository.save(client);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public void mouvementClient(@RequestBody Mouvement mouvement,
                                @PathVariable String id){
        clientRepository.save(clientRepository.getClientById(id).mouvement(mouvement));
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Client getClient(@PathVariable String id){
        return clientRepository.getClientById(id);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void removeClient(@PathVariable String id){
        clientRepository.deleteById(id);
    }

}

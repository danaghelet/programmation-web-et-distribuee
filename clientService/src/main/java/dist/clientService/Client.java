package dist.clientService;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Client {

    private long solde;
    private String id;
    private String name;

    public Client(long solde, String id, String name) {
        this.solde = solde;
        this.id = id;
        this.name = name;
    }

    public Client() {
        this.id = "";
        this.name = "";
        this.solde = 0;
    }

    public long getSolde() {
        return solde;
    }

    public void setSolde(long solde) {
        this.solde = solde;
    }

    @Id
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Client mouvement(Mouvement mouvement){
        if(mouvement.isVersement()){
            this.solde += mouvement.getMontant();
        }
        else{
            this.solde -= mouvement.getMontant();
        }

        return this;
    }
}

package dist.clientService;

import org.springframework.data.repository.CrudRepository;

public interface  ClientRepository extends CrudRepository<Client, String> {

    public Client getClientById(String id);
}

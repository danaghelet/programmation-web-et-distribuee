FROM ubuntu_java_mongodb
VOLUME /tmp
EXPOSE 27017
ENTRYPOINT ["mongod","--dbpath","/tmp","--port", "27017"]
